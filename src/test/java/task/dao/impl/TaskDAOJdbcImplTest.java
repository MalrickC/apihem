package task.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;

import Singletons.MyLogger;
import junit.framework.TestCase;
import task.model.Task;

public class TaskDAOJdbcImplTest extends TestCase {

	private TaskDAOJdbcImpl taskDAOJdbcImpl = null;
	
	// Get a logger session
	private MyLogger myLogger = MyLogger.getInstance();
	private Logger logger = myLogger.getLogger();

	@Test
	public void testInsert()
	{
		try {
			taskDAOJdbcImpl = new TaskDAOJdbcImpl();

			Task testCase = new Task(1, "TestTask", 5, true);

			taskDAOJdbcImpl.insert(testCase);

			assertTrue(taskDAOJdbcImpl.findById(1)!=null);

		} catch (ClassNotFoundException | SQLException e) {
			logger.debug(e.toString());
		}
	}
	
	@Test
	public void testDeleteById()
	{
		try {
			taskDAOJdbcImpl = new TaskDAOJdbcImpl();

			Task testCase = new Task(879657, "TestTask", 5, true);

			taskDAOJdbcImpl.deleteById(testCase.getId());

			assertTrue(taskDAOJdbcImpl.findById(879648)==null);

		} catch (ClassNotFoundException | SQLException e) {
			logger.debug(e.toString());
		}
	}
	
	@Test
	public void testDeleteAll()
	{
		try
		{
			taskDAOJdbcImpl = new TaskDAOJdbcImpl();
			
			for(int i=20; i<30; i++)
			{
				taskDAOJdbcImpl.insert(new Task(i, "JUnit test", i, true));
			}
			
			taskDAOJdbcImpl.deleteAll();
			
			assertTrue(taskDAOJdbcImpl.findAll().isEmpty());
		}
		catch (SQLException e) {
			logger.debug(e.toString());
		} catch (ClassNotFoundException e) {
			logger.debug(e.toString());
		}
	}
	
	@Test 
	public void testFindAll()
	{
		try {
			taskDAOJdbcImpl = new TaskDAOJdbcImpl();
			taskDAOJdbcImpl.deleteAll();
			for(int i=0; i<20; i++)
			{
				taskDAOJdbcImpl.insert(new Task(0 , "blabla", 4, true));
			}
			
			assertTrue(taskDAOJdbcImpl.findAll().size()==20);
			
			
		} catch (ClassNotFoundException e) {
			logger.debug(e.toString());
		} catch (SQLException e) {
			logger.debug(e.toString());
		}
	}
	
	@Test 
	public void testUpdate()
	{
		try {
			taskDAOJdbcImpl = new TaskDAOJdbcImpl();
		} catch (ClassNotFoundException | SQLException e) {
			logger.debug(e.toString());
		}
		int idmax = taskDAOJdbcImpl.findAll().size();
		int oldDuration = taskDAOJdbcImpl.findById(idmax).getDuration();
		
		taskDAOJdbcImpl.updateById(idmax, taskDAOJdbcImpl.findById(idmax).getName(), oldDuration+1, true);
		assertTrue(taskDAOJdbcImpl.findById(idmax).getDuration() == oldDuration +1);
		
	}
	
	@Test
	public void testFindByDone()
	{
		try {
			taskDAOJdbcImpl = new TaskDAOJdbcImpl();
		} catch (ClassNotFoundException | SQLException e) {
			logger.debug(e.toString());
		}
		
		List<Task> tasks = taskDAOJdbcImpl.findAll();
		
		int countDone=0;
		for(Task task : tasks)
		{
			if(task.isDone()) countDone++;
		}
		
		assertTrue(countDone == taskDAOJdbcImpl.findByDone(true).size());
		
		
	}

}
