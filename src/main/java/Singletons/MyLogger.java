package Singletons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Singleton used in order to get an only instance of the logger

public class MyLogger {

	private static Logger logger = LoggerFactory.getLogger(MyLogger.class);
	 
	private static MyLogger instance = null;
	
	public static MyLogger getInstance()
	{
		if(instance == null)
		{
			instance = new MyLogger();
		}
		return instance;
	}
	

	private MyLogger()
	{
		logger = LoggerFactory.getLogger(MyLogger.class);
	}


	public Logger getLogger() {
		return logger;
	}


	public static void setLogger(Logger logger) {
		MyLogger.logger = logger;
	}
	
	
	
}
