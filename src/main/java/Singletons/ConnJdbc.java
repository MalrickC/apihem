package Singletons;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;

// Singleton used in order to get an only connection for the whole application

public class ConnJdbc {
	
	private static ConnJdbc instance = new ConnJdbc();

	java.sql.Connection conn;
	
	// Get a logger session
	private MyLogger myLogger = MyLogger.getInstance();
	private Logger logger = myLogger.getLogger();
	
	
	public static ConnJdbc getInstance()
	{
		return instance;
	}
	
	public ConnJdbc()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			logger.debug(e.toString());
		}
		String DBurl = "jdbc:mysql://localhost:3306/myDB";
        String user = "root";
        String password = "";
        try {
			conn = DriverManager.getConnection(DBurl, user, password);
		} catch (SQLException e) {
			logger.debug(e.toString());
		}
	}

	public java.sql.Connection getConn() {
		return conn;
	}

	public void setConn(java.sql.Connection conn) {
		this.conn = conn;
	}
	
	
	
}
