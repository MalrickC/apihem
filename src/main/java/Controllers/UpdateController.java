package Controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import Singletons.MyLogger;
import task.dao.impl.TaskDAOJdbcImpl;

@Controller
@RequestMapping("/UPDATE")
public class UpdateController {

	TaskDAOJdbcImpl taskDAO;

	// Declaration of the logger 
	private MyLogger myLogger = MyLogger.getInstance();
	private Logger logger = myLogger.getLogger();

	@RequestMapping(method = RequestMethod.PUT)
	ModelAndView update(@RequestParam(value = "id", required = true) int id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "duration", required = true) int duration,
			@RequestParam(value = "done", required = true) boolean done) throws ServletException, IOException{
		
		// Declaration of the DAO 
		try {
			taskDAO = new TaskDAOJdbcImpl();
		} catch (ClassNotFoundException | SQLException e) {
			logger.debug(e.toString());
		}
		ModelAndView mv = new ModelAndView("update");
		taskDAO.updateById(id, name, duration, done);
		mv.addObject("result", taskDAO.findById(id));
		return mv;
	}

}
