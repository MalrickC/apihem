package Controllers;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import task.dao.TaskDAO;
import task.dao.impl.TaskDAOJdbcImpl;

import Singletons.MyLogger;


@Controller
@RequestMapping("/DELETE")
public class DeleteController {

	TaskDAO taskDAO;
	
	// Getting a logger session 
	private MyLogger myLogger = MyLogger.getInstance();
	private Logger logger = myLogger.getLogger();

	
	
	@RequestMapping(method = RequestMethod.DELETE)
	ModelAndView deleteById(@RequestParam(value = "id", required = false) String id)
	{
		
		ModelAndView mv = new ModelAndView("delete");
		
		/// Instanciation of the DAO
		try {
			taskDAO = new TaskDAOJdbcImpl();
		} catch (ClassNotFoundException | SQLException e) {
			logger.error(e.toString());
		}
		if(id==null)
		{
			mv.addObject("result", taskDAO.findAll());
			taskDAO.deleteAll();
		}
		else
		{
			mv.addObject("result", taskDAO.findById(Integer.valueOf(id)));
			taskDAO.deleteById(Integer.valueOf(id));
		}
		return mv;
	}
}
