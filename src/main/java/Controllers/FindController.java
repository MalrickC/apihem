package Controllers;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import Singletons.MyLogger;
import task.dao.TaskDAO;
import task.dao.impl.TaskDAOJdbcImpl;
import task.model.Task;

@Controller
@RequestMapping("/RETRIEVE")
public class FindController {

	TaskDAO taskDAO;
	
	// Get a logger session
	private MyLogger myLogger = MyLogger.getInstance();
	private Logger logger = myLogger.getLogger();


	@RequestMapping(method = RequestMethod.GET)
	ModelAndView find(@RequestParam(value = "id", required = false) String id)
	{
		/// Instanciation of the DAO
		try {
			taskDAO = new TaskDAOJdbcImpl();
		} catch (ClassNotFoundException | SQLException e) {
			logger.error(e.toString());
		}
		ModelAndView mv = new ModelAndView("find");

		
		/// If no id is specified, then return all results, and log the tasks.
		
		if(id==null)
		{
			for(Task task : taskDAO.findAll())
			{
				logger.info(task.toString());

			}

			mv.addObject("result", taskDAO.findAll());
		}
		else 
		{
			/// If an id is specified, then return an element and log the task
			
			List<Task> result = new ArrayList<Task>();
			result.add(taskDAO.findById(Integer.valueOf(id)));
			logger.info(taskDAO.findById(Integer.valueOf(id)).toString());
			mv.addObject("result", result);
		}
		return mv;
	}
	
	@RequestMapping("/FindByDone")
	ModelAndView findByDone(@RequestParam(value = "done", required = true) boolean done)
	{
		// Instanciation of the DAO 
		try {
			taskDAO = new TaskDAOJdbcImpl();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		ModelAndView mv = new ModelAndView("FindByDone");
		mv.addObject("doneSelection", taskDAO.findByDone(done));
		
		return mv;	
		
	}
}
