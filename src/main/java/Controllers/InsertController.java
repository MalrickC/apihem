package Controllers;
import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import task.dao.TaskDAO;
import task.dao.impl.TaskDAOJdbcImpl;
import task.model.Task;

@Controller
@RequestMapping("/INSERT")
public class InsertController {

	TaskDAO taskDAO;

	
	@RequestMapping(method = RequestMethod.POST)
	ModelAndView insert(@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "duration", required = true) int duration,
			@RequestParam(value = "done", required = true) boolean done
			)
	{
		ModelAndView mv = new ModelAndView("insert");
		
		// Instanciation of the  DDAO 
		try {
			taskDAO = new TaskDAOJdbcImpl();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		Task task = new Task(0, name, duration, done);
		
		/// Insersion, and sending the insered task to the view
		int id = taskDAO.insert(task);
		mv.addObject("result", taskDAO.findById(id));

		return mv;
	}


}
