package task.model;

public class Task {

	int id;

	String name;

	int duration; 

	boolean done;

	public Task()
	{

	}

	public Task(int id, String name, int duration, boolean done) {
		super();
		this.id = id;
		this.name = name;
		this.duration = duration;
		this.done = done;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", name=" + name + ", duration=" + duration + ", done=" + done + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + duration;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
		{
			System.out.println("b");
			return false;
		}
		if (getClass() != obj.getClass())
		{
			System.out.println("b");
			return false;
		}
		Task other = (Task) obj;
		if (duration != other.duration)
		{
			System.out.println("c");
			return false;
		}
		if (id != other.id)
		{
			System.out.println("d");
			return false;
		}
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


}
