package task.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import task.model.Task;;

/// Interface defining the functions of the DAO

@Repository
public interface TaskDAO {

	public int insert(Task task);
	
	public Task findById(int id);
	
	public List<Task> findAll();
	
	void deleteById(int id);
	
	void deleteAll();

	void updateById(int id, String name, int duration, boolean done);

	public List<Task> findByDone(boolean done);


}
