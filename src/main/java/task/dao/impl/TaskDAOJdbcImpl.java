package task.dao.impl;

import Singletons.ConnJdbc;
import Singletons.MyLogger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import task.dao.TaskDAO;
import task.model.Task;

// Implementation of the DAO contract, in JDBC 

@Component
public class TaskDAOJdbcImpl implements TaskDAO {

	/// Connection, logger
	private ConnJdbc conn;
	private MyLogger myLogger;
	private Logger logger;

	
	public TaskDAOJdbcImpl() throws ClassNotFoundException, SQLException
	{
		/// Instanciation of local variables
		conn = ConnJdbc.getInstance();
		myLogger = MyLogger.getInstance();
		logger = myLogger.getLogger();
	}


	public int insert(Task task){
		int id;
		try {
			// Recuperation of the maximum id (No auto-increment on DB side)
			ResultSet rs;
			String sql = "INSERT INTO Task (id, name, duration) VALUES (?, ?, ?)";
			String idSql = "SELECT MAX(ID) FROM TASK";

			Statement st;
			st = conn.getConn().createStatement();
			rs = st.executeQuery(idSql);
			rs.next();
			id =0;
			id=rs.getInt(1);
			
			/// Realization of the insertion
			PreparedStatement ps = conn.getConn().prepareStatement(sql);
			ps.setInt(1, id+1); 
			ps.setString(2, task.getName());
			ps.setInt(3, task.getDuration());
			ps.executeUpdate();
			ps.close();
			logger.info("One task has been inserted");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return id;

	}

	@Override
	public Task findById(int id){
		String sql = "SELECT * from TASK WHERE id = ?";
		try {
			PreparedStatement ps = conn.getConn().prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				logger.info("One element has been selected");
				return new Task(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getBoolean(4));

			}
			rs.close();
			ps.close();

		} catch (SQLException e) {
			logger.error(e.toString());
		}
		logger.info("An element has not been found");
		return null;
	}


	@Override
	public void deleteById(int id) {
		String sql = "DELETE FROM TASK WHERE ID = ?";

		try {
			PreparedStatement ps = conn.getConn().prepareStatement(sql);
			ps.setInt(1, id);
			ps.execute();
		} catch (SQLException e) {
			logger.error(e.toString());
		}
		logger.info("One element has been deleted");

	}

	@Override
	public void deleteAll()
	{
		String sql = "delete FROM TASK";
		try {
			Statement st = conn.getConn().createStatement();
			st.execute(sql);
		} catch (SQLException e) {
			logger.error(e.toString());
		}
		logger.info("All the elements have been deleted");

	}



	@Override
	public List<Task> findAll() {
		String sql = "SELECT * FROM TASK";
		List<Task> result = new ArrayList<>();
		try {
			Statement st = conn.getConn().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
			{
				result.add(new Task(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getBoolean(4)));
			}
		} catch (SQLException e) {
			logger.error(e.toString());
		}
		logger.info("All the elements have been selected");
		return result;
	}


	@Override
	public void updateById(int id, String name, int duration, boolean done) {
		String sql = "UPDATE TASK SET name = ?, duration = ?, done = ? WHERE id = ?";

		try {
			PreparedStatement ps = conn.getConn().prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, duration);
			ps.setBoolean(3, done);
			ps.setInt(4, id);
			ps.executeUpdate();
			logger.info("An element has been updated");
		} catch (SQLException e) {
			logger.error(e.toString());
		}


	}


	@Override
	public List<Task> findByDone(boolean done) {
		String sql = "SELECT * FROM TASK WHERE DONE = ?";
		
		try {
			PreparedStatement ps = conn.getConn().prepareStatement(sql);
			ps.setBoolean(1, done);
			ResultSet rs= ps.executeQuery();
			List<Task> result = new ArrayList<Task>();
			
			while(rs.next())
			{
				result.add(new Task(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getBoolean(4)));
				System.out.println("coucou");
			}
			return result;
		} catch (SQLException e) {
			logger.error(e.toString());
		}
		
		
		return null;
	}
	



}
